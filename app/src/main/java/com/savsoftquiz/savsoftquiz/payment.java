package com.savsoftquiz.savsoftquiz;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.support.v7.widget.Toolbar;

public class payment extends AppCompatActivity {


    private String app_url;
    private String app_url2="index.php/";
    private String api_app_key="savsoft";
    private WebView mWebView;


    @Override
    public void onBackPressed() {

        if(mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        app_url=getString(R.string.base_url);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        Bundle extras = getIntent().getExtras();

        String uid = extras.getString("uid");

        mWebView = (WebView) findViewById(R.id.activity_main_webview);

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);


        mWebView.addJavascriptInterface(this, "Android");



        //  Toast.makeText(AttemptQuiz.this, rid, Toast.LENGTH_SHORT).show();
        String url = app_url.concat(app_url2).concat("payment_gateway/subscription_expired/").concat(uid);
        //  String url = app_url;
        mWebView.loadUrl(url);

        mWebView.setWebViewClient(new WebViewClient());





    }
}
