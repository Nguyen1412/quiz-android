package com.savsoftquiz.savsoftquiz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private String app_url;
    private String app_url2="index.php/";
    private String api_app_key="savsoft";

    private static final String LOCALE_KEY = "localekey";
    private static final String HINDI_LOCALE = "hi";
    private static final String ENGLISH_LOCALE = "en_US";
    private static final String LOCALE_PREF_KEY = "localePref";


    private Locale locale;

    TextView t_quiz,t_quiz_attempt,t_quiz_pass,t_quiz_fail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        app_url = getString(R.string.base_url);

        t_quiz = (TextView) findViewById(R.id.no_quiz);
        t_quiz_attempt = (TextView) findViewById(R.id.no_quiz_attempted);
        t_quiz_pass = (TextView) findViewById(R.id.no_quiz_pass);
        t_quiz_fail = (TextView) findViewById(R.id.no_quiz_fail);

        if (CommonFunctions.isnetworkavailable(DashboardActivity.this)) {
            get_stats();
        } else {
            Toast.makeText(this, getString(R.string.internet_message), Toast.LENGTH_SHORT).show();

        }

        setTitle(R.string.title_activity_dashboard);

//        Fetching sharedpreferences to get Locale stored in them


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void get_stats() {


        SharedPreferences prefs = getSharedPreferences("sq_user", MODE_PRIVATE);
        final String suid = prefs.getString("uid", null);
        final String connection_key = prefs.getString("connection_key", null);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = app_url.concat(app_url2).concat("api/stats/") .concat(connection_key).concat("/").concat(suid);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject  jsonRootObject = new JSONObject(response);

                           String no_quiz=  jsonRootObject.getString("no_quiz");
                           String no_attempted=  jsonRootObject.getString("no_attempted");
                           String no_pass=  jsonRootObject.getString("no_pass");
                           String no_fail=  jsonRootObject.getString("no_fail");
                            t_quiz.setText(no_quiz);
                            t_quiz_attempt.setText(no_attempted);
                            t_quiz_pass.setText(no_pass);
                            t_quiz_fail.setText(no_fail);



                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(DashboardActivity.this, getString(R.string.internet_message), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DashboardActivity.this, getString(R.string.internet_message), Toast.LENGTH_SHORT).show();

                //  mTextView.setText("Error:getresponse");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);



    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {


        } else if (id == R.id.quiz_list) {
            // Handle the camera action
            Intent i=new Intent(DashboardActivity.this,QuizList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.results) {

            Intent i=new Intent(DashboardActivity.this,ResutList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.my_account) {

            Intent i=new Intent(DashboardActivity.this,MyAccount.class);
            startActivity(i);
            finish();


        } else if (id == R.id.logout) {


            // create shared preferences
            SharedPreferences.Editor editor = getSharedPreferences("sq_user", MODE_PRIVATE).edit();

            editor.putString("logged_in", "0");
            editor.putString("uemail", "");
            editor.putString("uid", "");
            editor.putString("first_name", "");
            editor.putString("last_name", "");
            editor.putString("contact_no", "");
            editor.putString("gid", "");
            editor.putString("connection_key", "");
            editor.commit();

            Intent i = new Intent(DashboardActivity.this, Login3Activity.class);
            startActivity(i);
            finish();


        } else if (id == R.id.nav_share) {


            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, app_url+" ");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));


        }else if (id == R.id.notifications) {

            Intent i = new Intent(DashboardActivity.this, NotificationActivity.class);
            startActivity(i);




        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
