package com.savsoftquiz.savsoftquiz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MyAccount extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private String app_url;
    private String app_url2="index.php/";
    private String api_app_key="savsoft";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        app_url=getString(R.string.base_url);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Button mEmailSignInButton = (Button) findViewById(R.id.submit_myaccount);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(CommonFunctions.isnetworkavailable(MyAccount.this)){
                    submitform();
                }else{
                    Toast.makeText(MyAccount.this, getString(R.string.internet_message), Toast.LENGTH_SHORT).show();

                }

            }
        });

        SharedPreferences prefs = getSharedPreferences("sq_user", MODE_PRIVATE);
        final String suid = prefs.getString("uid", null);
        final String connection_key = prefs.getString("connection_key", null);
        final String first_name = prefs.getString("first_name", null);
        final String last_name = prefs.getString("last_name", null);

        TextView first_name2 = (TextView)findViewById(R.id.first_name);
        first_name2.setText(first_name);
        TextView last_name2 = (TextView)findViewById(R.id.last_name);
        last_name2.setText(last_name);




    }


    private void submitform() {

        SharedPreferences prefs = getSharedPreferences("sq_user", MODE_PRIVATE);
        final String suid = prefs.getString("uid", null);
        final String connection_key = prefs.getString("connection_key", null);



        String efname = null;
        try {
            EditText f_fn   = (EditText)findViewById(R.id.first_name);
            efname=f_fn.getText().toString();
            efname = URLEncoder.encode(efname,"UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String elname = null;
        try {
            EditText l_fn   = (EditText)findViewById(R.id.last_name);
            elname=l_fn.getText().toString();
            elname = URLEncoder.encode(elname,"UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String epass = null;
        try {
            EditText l_p   = (EditText)findViewById(R.id.password);
            epass=l_p.getText().toString();
            epass = URLEncoder.encode(epass,"UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = app_url.concat(app_url2).concat("api/myaccount/") .concat(connection_key).concat("/").concat(suid).concat("/").concat(efname).concat("/").concat(elname).concat("/").concat(epass).concat("/");

        // create shared preferences
        SharedPreferences.Editor editor = getSharedPreferences("sq_user", MODE_PRIVATE).edit();
        editor.putString("first_name", efname);
        editor.putString("last_name", elname);
        editor.commit();
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(MyAccount.this, response, Toast.LENGTH_SHORT).show();





                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MyAccount.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });


        queue.add(stringRequest);




    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
            // Handle the camera action
            Intent i=new Intent(MyAccount.this,QuizList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.quiz_list) {
            // Handle the camera action
            Intent i=new Intent(MyAccount.this,QuizList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.results) {

            Intent i=new Intent(MyAccount.this,ResutList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.my_account) {

            Intent i=new Intent(MyAccount.this,MyAccount.class);
            startActivity(i);
            finish();


        } else if (id == R.id.logout) {


            // create shared preferences
            SharedPreferences.Editor editor = getSharedPreferences("sq_user", MODE_PRIVATE).edit();
            editor.putString("uemail", "");
            editor.putString("uid", "");
            editor.putString("first_name", "");
            editor.putString("last_name", "");
            editor.putString("contact_no", "");
            editor.putString("gid", "");
            editor.putString("connection_key", "");
            editor.commit();

            Intent i = new Intent(MyAccount.this, Login3Activity.class);
            startActivity(i);
            finish();


        } else if (id == R.id.nav_share) {


            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, app_url+" ");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));


        }else if (id == R.id.notifications) {

            Intent i = new Intent(MyAccount.this, NotificationActivity.class);
            startActivity(i);




        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
