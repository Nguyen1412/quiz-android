package com.savsoftquiz.savsoftquiz;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

public class ResultView extends AppCompatActivity {


    private String app_url;
    private String app_url2="index.php/";
    private String api_app_key="savsoft";
    private WebView mWebView;


    @Override
    public void onBackPressed() {

        if(mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        app_url=getString(R.string.base_url);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        Bundle extras = getIntent().getExtras();

        String rid = extras.getString("rid");
        //The key argument here must match that used in the other activity
        SharedPreferences prefs = getSharedPreferences("sq_user", MODE_PRIVATE);
        final String suid = prefs.getString("uid", null);
        final String connection_key = prefs.getString("connection_key", null);

        mWebView = (WebView) findViewById(R.id.activity_main_webview);

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);


        //mWebView.addJavascriptInterface(this, "Android");



        //  Toast.makeText(AttemptQuiz.this, rid, Toast.LENGTH_SHORT).show();
        String url = app_url.concat(app_url2).concat("api/view_result/").concat(connection_key).concat("/").concat(suid).concat("/").concat(rid);
        //  String url = app_url;
       // mWebView.loadUrl(url);

       // mWebView.setWebViewClient(new WebViewClient());


        final ProgressDialog progressBar = ProgressDialog.show(ResultView.this, "", "Loading...");

        mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //Log.i(TAG, "Processing webview url click...");
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                //Log.i(TAG, "Finished loading URL: " +url);
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                // Log.e(TAG, "Error: " + description);
                Toast.makeText(ResultView.this, getString(R.string.internet_title), Toast.LENGTH_SHORT).show();

            }
        });
        mWebView.loadUrl(url);





    }
}
