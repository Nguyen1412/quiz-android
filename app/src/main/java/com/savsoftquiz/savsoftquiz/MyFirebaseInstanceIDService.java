package com.savsoftquiz.savsoftquiz;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by savsoft-3 on 21/4/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {


        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if(refreshedToken.equals("") || refreshedToken.equals(" ")){
            onTokenRefresh();
        }else{
            Log.d(TAG, "Refreshed token: " + refreshedToken);
            SharedPreferences.Editor editor = getSharedPreferences("notification_token", MODE_PRIVATE).edit();
            editor.putString("token_registered", refreshedToken);
            editor.commit();

        }


    }

}
