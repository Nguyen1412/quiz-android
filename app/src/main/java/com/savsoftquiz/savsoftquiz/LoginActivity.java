package com.savsoftquiz.savsoftquiz;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.google.firebase.messaging.FirebaseMessaging;

public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);


        // Make this activity, full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Hide the Title bar of this activity screen
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);




        setContentView(R.layout.activity_login);


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                SharedPreferences log_sesion = getSharedPreferences("sq_user", MODE_PRIVATE);
                String logged_in = log_sesion.getString("logged_in", "0");

                if(CommonFunctions.isnetworkavailable(LoginActivity.this)){
                    FirebaseMessaging.getInstance().subscribeToTopic("SavsoftQuiz");

                }


                if(logged_in.equals("1") ){
                    Intent inr = new Intent(LoginActivity.this, DashboardActivity.class);
                    startActivity(inr);
                    finish();

                }else{

                    Intent i=new Intent(LoginActivity.this,Login3Activity.class);
                    startActivity(i);
                    finish();
                }


            }
        }, 2500);




    }


}
