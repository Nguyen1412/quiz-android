package com.savsoftquiz.savsoftquiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class QuizList extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String app_url;
    private String app_url2="index.php/";
    private String api_app_key="savsoft";

    public static final String FIRST_COLUMN="First";
    public static final String SECOND_COLUMN="Second";
    public static final String THIRD_COLUMN="Third";
    public static final String FOURTH_COLUMN="Fourth";
public String[] quids = new String[30];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_list);
        app_url=getString(R.string.base_url);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final TextView mTextView = (TextView) findViewById(R.id.errormsg);

        setTitle(R.string.title_activity_quiz_list);
        SharedPreferences prefs = getSharedPreferences("sq_user", MODE_PRIVATE);
        final String suid = prefs.getString("uid", null);
        final String connection_key = prefs.getString("connection_key", null);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = app_url.concat(app_url2).concat("api/quiz_list/") .concat(connection_key).concat("/").concat(suid).concat("/0");

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        // Toast.makeText(QuizList.this,response,Toast.LENGTH_LONG).show();

    // list view calls

                        ListView listView=(ListView)findViewById(R.id.listView1);

                        ArrayList<HashMap<String,String>> list=new ArrayList<HashMap<String,String>>();


// Toast.makeText(QuizList.this,response,Toast.LENGTH_LONG).show();

    // list view calls




                        if(response.trim().equals("")){
                            mTextView.setText("No quiz Found!");
                          //  mTextView.setText(response);
                           // showProgress(false);
                        }

                        try {
                            JSONObject  jsonRootObject = new JSONObject(response);

                            //Get the instance of JSONArray that contains JSONObjects
                            JSONArray jsonArray = jsonRootObject.optJSONArray("quiz");

                            for(int i=0; i < jsonArray.length(); i++){


                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                quids[i]=jsonObject.optString("quid").toString();
                                String quid = jsonObject.optString("quid").toString();
                                String duration = jsonObject.optString("duration").toString();
                                String name = jsonObject.optString("quiz_name").toString();
                                String noq = jsonObject.optString("noq").toString();
                                String max_attempt = jsonObject.optString("maximum_attempts").toString();
                                HashMap<String,String> temp=new HashMap<String, String>();
                                //float salary = Float.parseFloat(jsonObject.optString("salary").toString());
                                temp.put(FIRST_COLUMN, name);
                                temp.put(SECOND_COLUMN, duration);
                                temp.put("noq", noq);
                                temp.put("max_attempt", max_attempt);



                                list.add(temp);
                            }

                        } catch (JSONException e) {e.printStackTrace();}

                        ListViewAdapter adapter1 =new ListViewAdapter(QuizList.this, list);
                        listView.setAdapter(adapter1);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(AdapterView<?> parent, final View view, int position, long id)
                            {
                                int pos=position;
                                Toast.makeText(QuizList.this, " Validating...", Toast.LENGTH_SHORT).show();

                                // http request to validate


                                String quid = quids[pos];

                                RequestQueue queue = Volley.newRequestQueue(QuizList.this);
                                String url = app_url.concat(app_url2).concat("api/validate_quiz/").concat(connection_key).concat("/").concat(suid).concat("/").concat(quid);

// Request a string response from the provided URL.
                                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {





                                                try {
                                                    int num = Integer.parseInt(response.trim());


                                                    Intent i = new Intent(QuizList.this, AttemptQuiz.class);
                                                    i.putExtra ("rid",response);
                                                    startActivity(i);


                                                   // Toast.makeText(QuizList.this, response, Toast.LENGTH_SHORT).show();

                                                   // mTextView.setText(response);

                                                } catch (NumberFormatException e) {

                                                    AlertDialog alertDialog = new AlertDialog.Builder(QuizList.this).create();
                                                    alertDialog.setTitle("You can not attempt this quiz:");
                                                    alertDialog.setMessage(response);
                                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    dialog.dismiss();
                                                                }
                                                            });
                                                    alertDialog.show();

                                                    //Toast.makeText(QuizList.this, response, Toast.LENGTH_SHORT).show();

                                                   // mTextView.setText(response);
                                                }








                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        mTextView.setText("Error:getresponse");
                                        Toast.makeText(QuizList.this, "Error", Toast.LENGTH_SHORT).show();
                                    }
                                });

                                // Add the request to the RequestQueue.
                                queue.add(stringRequest);


                                // http request ends








                            }

                        });

      // list view ends



                        if(response.trim().equals("")){
                            mTextView.setText("No quiz Found!");
                          //  mTextView.setText(response);
                           // showProgress(false);
                        }     }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               //  mTextView.setText("Error:getresponse");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }












/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.quiz_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
            // Handle the camera action
            Intent i=new Intent(QuizList.this,DashboardActivity.class);
            startActivity(i);
            finish();

        } else  if (id == R.id.quiz_list) {
            // Handle the camera action
            Intent i=new Intent(QuizList.this,QuizList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.results) {

            Intent i=new Intent(QuizList.this,ResutList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.my_account) {

            Intent i=new Intent(QuizList.this,MyAccount.class);
            startActivity(i);
            finish();

        } else if (id == R.id.logout) {


            // create shared preferences
            SharedPreferences.Editor editor = getSharedPreferences("sq_user", MODE_PRIVATE).edit();
            editor.putString("logged_in", "0");
            editor.putString("uemail", "");
            editor.putString("uid", "");
            editor.putString("first_name", "");
            editor.putString("last_name", "");
            editor.putString("contact_no", "");
            editor.putString("gid", "");
            editor.putString("connection_key", "");
            editor.commit();

            Intent i = new Intent(QuizList.this, Login3Activity.class);
            startActivity(i);
            finish();


        } else if (id == R.id.nav_share) {


            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, app_url+" ");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));


        }else if (id == R.id.notifications) {

            Intent i = new Intent(QuizList.this, NotificationActivity.class);
            startActivity(i);




        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }













































}

















