package com.savsoftquiz.savsoftquiz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private String app_url;
    private String app_url2="index.php/";
    private String api_app_key="savsoft";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        app_url=getString(R.string.base_url);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
if(CommonFunctions.isnetworkavailable(NotificationActivity.this)){
    getNotifications();

}else{

    Toast.makeText(this, getString(R.string.internet_message), Toast.LENGTH_SHORT).show();
}



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void getNotifications() {


        SharedPreferences prefs = getSharedPreferences("sq_user", MODE_PRIVATE);
        final String suid = prefs.getString("uid", null);
        final String connection_key = prefs.getString("connection_key", null);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = app_url.concat(app_url2).concat("api/get_notification/") .concat(connection_key).concat("/").concat(suid);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                       //  CommonFunctions.show_dialog(NotificationActivity.this,"",response);

                        try {
                            JSONObject result = new JSONObject(response);
                            JSONArray notifications = result.getJSONArray("result");

                            LinearLayout options_layout = (LinearLayout) findViewById(R.id.notification_list);
                            options_layout.removeAllViews();
                            for (int i=0;i<notifications.length();i++){
                                LayoutInflater inflater = LayoutInflater.from(NotificationActivity.this);
                                View to_add = inflater.inflate(R.layout.notification_element,
                                        options_layout,false);

                                JSONObject ind_notification = notifications.getJSONObject(i);
                                TextView not_msg = (TextView) to_add.findViewById(R.id.notification_msg);
                                TextView notification_title = (TextView) to_add.findViewById(R.id.notification_title);
                                TextView not_date = (TextView) to_add.findViewById(R.id.notification_date);
                                // TextView comments = (TextView) to_add.findViewById(R.id.user_comments);
                            /*    long unixSeconds = Long.parseLong(ind_notification.optString("notification_date"));
                                Date date = new Date(unixSeconds*1000L); // *1000 is to convert seconds to milliseconds
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // the format of your date
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone reference for formating (see comment at the bottom
                                String formattedDate = sdf.format(date);*/
                                not_msg.setText(ind_notification.optString("message"));
                                not_date.setText(ind_notification.optString("notification_date"));
                                notification_title.setText(ind_notification.optString("title"));

                                options_layout.addView(to_add);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  mTextView.setText("Error:getresponse");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.dashboard) {
            // Handle the camera action
            Intent i=new Intent(NotificationActivity.this,DashboardActivity.class);
            startActivity(i);
            finish();

        } else  if (id == R.id.quiz_list) {
            // Handle the camera action
            Intent i=new Intent(NotificationActivity.this,QuizList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.results) {
            Intent i=new Intent(NotificationActivity.this,ResutList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.my_account) {

            Intent i=new Intent(NotificationActivity.this,MyAccount.class);
            startActivity(i);
            finish();

        } else if (id == R.id.logout) {



            // create shared preferences
            SharedPreferences.Editor editor = getSharedPreferences("sq_user", MODE_PRIVATE).edit();
            editor.putString("logged_in", "0");
            editor.putString("uemail", "");
            editor.putString("uid", "");
            editor.putString("first_name", "");
            editor.putString("last_name", "");
            editor.putString("contact_no", "");
            editor.putString("gid", "");
            editor.putString("connection_key", "");
            editor.commit();

            Intent i = new Intent(NotificationActivity.this, Login3Activity.class);
            startActivity(i);
            finish();


        } else if (id == R.id.nav_share) {



            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, app_url+" ");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));


        }else if (id == R.id.notifications) {

           /* Intent i = new Intent(NotificationActivity.this, NotificationActivity.class);
            startActivity(i);*/




        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
