package com.savsoftquiz.savsoftquiz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ResutList extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String app_url;
    private String app_url2="index.php/";
    private String api_app_key="savsoft";

    public static final String FIRST_COLUMN="First";
    public static final String SECOND_COLUMN="Second";
    public static final String THIRD_COLUMN="Third";
    public static final String FOURTH_COLUMN="Fourth";
    public String[] rids = new String[30];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resut_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        app_url=getString(R.string.base_url);






        // content


        final TextView mTextView = (TextView) findViewById(R.id.errormsg);


        SharedPreferences prefs = getSharedPreferences("sq_user", MODE_PRIVATE);
        final String suid = prefs.getString("uid", null);
        final String connection_key = prefs.getString("connection_key", null);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = app_url.concat(app_url2).concat("api/result_list/") .concat(connection_key).concat("/").concat(suid).concat("/0");

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        // Toast.makeText(QuizList.this,response,Toast.LENGTH_LONG).show();

                        // list view calls

                        ListView listView=(ListView)findViewById(R.id.listView1);

                        ArrayList<HashMap<String,String>> list=new ArrayList<HashMap<String,String>>();


 // Toast.makeText(ResutList.this,response,Toast.LENGTH_LONG).show();

                        // list view calls




                        if(response.trim().equals("")){
                            mTextView.setText("No result Found!");
                            //  mTextView.setText(response);
                            // showProgress(false);
                        }

                        try {
                            JSONObject jsonRootObject = new JSONObject(response);

                            //Get the instance of JSONArray that contains JSONObjects
                            JSONArray jsonArray = jsonRootObject.optJSONArray("result");

                            for(int i=0; i < jsonArray.length(); i++){


                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                rids[i]=jsonObject.optString("rid").toString();
                                String result_status = jsonObject.optString("result_status").toString();
                                String percentage_obtained = jsonObject.optString("percentage_obtained").toString();
                                String name = jsonObject.optString("quiz_name").toString();
                                HashMap<String,String> temp=new HashMap<String, String>();
                                //float salary = Float.parseFloat(jsonObject.optString("salary").toString());
                                temp.put(FIRST_COLUMN, name);
                                temp.put(SECOND_COLUMN, "Status: "+result_status+" ");
                                temp.put(THIRD_COLUMN, "Score: "+percentage_obtained+"%");


                                list.add(temp);
                            }

                        } catch (JSONException e) {e.printStackTrace();}

                        ListViewAdapter2 adapter1 = new ListViewAdapter2(ResutList.this, list);
                        listView.setAdapter(adapter1);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(AdapterView<?> parent, final View view, int position, long id)
                            {
                                int pos=position;
                                Toast.makeText(ResutList.this, " Loading...", Toast.LENGTH_SHORT).show();

                                // http request to validate


                                String rid = rids[pos];




                                                    Intent i = new Intent(ResutList.this, ResultView.class);
                                                    i.putExtra ("rid",rid);
                                                    startActivity(i);

















                            }

                        });

                        // list view ends



                        if(response.trim().equals("")){
                            mTextView.setText("No Result Found!");
                            //  mTextView.setText(response);
                            // showProgress(false);
                        }     }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  mTextView.setText("Error:getresponse");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);







        // content ends






        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.resut_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
            // Handle the camera action
            Intent i=new Intent(ResutList.this,DashboardActivity.class);
            startActivity(i);
            finish();

        } else  if (id == R.id.quiz_list) {
            // Handle the camera action
            Intent i=new Intent(ResutList.this,QuizList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.results) {
            Intent i=new Intent(ResutList.this,ResutList.class);
            startActivity(i);
            finish();

        } else if (id == R.id.my_account) {

            Intent i=new Intent(ResutList.this,MyAccount.class);
            startActivity(i);
            finish();

        } else if (id == R.id.logout) {



            // create shared preferences
            SharedPreferences.Editor editor = getSharedPreferences("sq_user", MODE_PRIVATE).edit();
            editor.putString("logged_in", "0");
            editor.putString("uemail", "");
            editor.putString("uid", "");
            editor.putString("first_name", "");
            editor.putString("last_name", "");
            editor.putString("contact_no", "");
            editor.putString("gid", "");
            editor.putString("connection_key", "");
            editor.commit();

            Intent i = new Intent(ResutList.this, Login3Activity.class);
            startActivity(i);
            finish();


        } else if (id == R.id.nav_share) {



            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
             sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, app_url+" ");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));


        }else if (id == R.id.notifications) {

            Intent i = new Intent(ResutList.this, NotificationActivity.class);
            startActivity(i);




        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
