package com.savsoftquiz.savsoftquiz;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by gurpinder on 7/6/16.
 */
class ListViewAdapter extends BaseAdapter {

    public static final String FIRST_COLUMN="First";
    public static final String SECOND_COLUMN="Second";
    public static final String THIRD_COLUMN="Third";
    public static final String FOURTH_COLUMN="Fourth";

    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    TextView txtFirst;
    TextView txtSecond;
    TextView noq;
    TextView max_attempts;
    public ListViewAdapter(Activity activity, ArrayList<HashMap<String, String>> list){
        super();
        this.activity=activity;
        this.list=list;
    }
    @Override
    public int getCount() {
// TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
// TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
// TODO Auto-generated method stub
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
        LayoutInflater inflater=activity.getLayoutInflater();
        if(convertView == null){
            convertView=inflater.inflate(R.layout.colmn_row, null);
            txtFirst=(TextView) convertView.findViewById(R.id.quiz_name);
            txtSecond=(TextView) convertView.findViewById(R.id.duration);
            noq=(TextView) convertView.findViewById(R.id.noq);
            max_attempts=(TextView) convertView.findViewById(R.id.max_attempts);
        }
        HashMap<String, String> map=list.get(position);
        txtFirst.setText(map.get(FIRST_COLUMN));
        txtSecond.setText(map.get(SECOND_COLUMN));
        noq.setText(map.get("noq"));
        max_attempts.setText(map.get("max_attempt"));
        return convertView;
    }

}